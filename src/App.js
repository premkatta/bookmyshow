import React, { Component } from 'react';
import {BrowserRouter, Route , Switch, NavLink} from 'react-router-dom';

import logo from './logo.svg';
import './App.css';

import Home from './components/home';
import Navbar from './components/navbar';
import Carousel from './components/carousel';
import MovieCards from './components/movieCards';
import MoviesAll from './components/moviesAll';
import FooterPanel from './components/footerPanel';
import Quickbook from './components/quickbook';
import Welcome from './components/welcome';
import MovieDetailedInfo from './components/movieDetailedInfo';
import Theatres from './components/theatres';
import Seatbooking from './components/seatbooking';




class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <div className="App">   
            
            <Route path ="/" strict exact component={Home}/>
            <Route path = "/quickbook" strict exact component = {Quickbook}/>
            <Route path = "/movies" strict exact component= {MoviesAll}/>
            <Route path = "/movies/:movie_name" strict exact component ={MovieDetailedInfo}/>
            <Route path ="/movies/:movie_name/theatres/seatbooking" exact component = {Seatbooking}/>
            <Route path = "/movies/:movie_name/theatres/" exact component = {Theatres}/>

      </div>
      </BrowserRouter>
       
    );
  }
}

export default App;
