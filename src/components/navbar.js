import React , {Component} from 'react';
import '../css/navbar.css';

import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import {NavLink} from 'react-router-dom';



class Navbar extends Component{
    state = {
        open: false,
      };
    
      handleClickOpen = () => {
        this.setState({ open: true });
      };
    
      handleClose = () => {
        this.setState({ open: false });
      };

      state = {
        right: false,
      };
    
      toggleDrawer = (side, open) => () => {
        this.setState({
          [side]: open,
        });
      };

    render(){
    const { classes } = this.props;
    const sideList = (
      <div className>
        <div className = "Guest">
          <span class = "userIcon">
            <img src = {require("../images/user.svg")}/>
          </span>
          <span class ="welGuest">
            <p className = "wg">Welcome Guest!</p>
            <p>Sign In / Sign Up</p>
          </span>
        </div>
        <List>
          <ListItem button >
            Notifications
          </ListItem>
        </List>
        <List>
          <ListItem button >
            Support
          </ListItem>
        </List>
        <List>
          <ListItem button >
            Settings
          </ListItem>
        </List>
        <Divider/>  
      </div>
    );
        return(
        <div>
          
            <div>
                <Drawer anchor="right" open={this.state.right} onClose={this.toggleDrawer('right', false)}>
                <div
                    tabIndex={0}
                    role="button"
                    onClick={this.toggleDrawer('right', false)}
                    onKeyDown={this.toggleDrawer('right', false)}
                >
                    {sideList}
                </div>
                </Drawer>
            </div>

            <div className = "signin">
            <Dialog
            open={this.state.open}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
            >
          <DialogTitle id="form-dialog-title">
          <a className ="login_center">Login</a> 
            <div class = "loginX" onClick={this.handleClose }>X</div>
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
           <div><button type = "button" className = "signbtn btn btn-primary fb-color btn-ht"><a className="fa_signin"><img src ={require("../images/fb_signin.svg" )}/></a> Connect with Facebook</button></div> 
           <div><a href = "https://accounts.google.com/signin/oauth/oauthchooseaccount?client_id=990572338172-iibth2em4l86htv30eg1v44jia37fuo5.apps.googleusercontent.com&as=j_2dSbUzMA3XnrzuxHB9CQ&destination=https%3A%2F%2Fin.bookmyshow.com&approval_state=!ChRjQTNtNUdhYnNtM09hWDRkd2JNahIfWV9oWEVwU05XSGtWOEhuU1JuY2dubXBPd2dsLWdoWQ%E2%88%99APNbktkAAAAAXDR8quAlAqLGrYaKco7m2jt_5jvzf5-e&oauthgdpr=1&xsrfsig=AHgIfE-BAFniARa3BtosRIjLrzo8KPYc3w&flowName=GeneralOAuthFlow"> <button type = "button" className = "signbtn btn btn-light btn-outline-secondary btn-ht"><a className="google_signin"><img src={require("../images/google.svg")}/></a>Connect with Google</button></a></div>
           <div className = "signup" ><a>Dont have an account?</a><a className = "sColor">signup</a></div>
            </DialogContentText>
            <div className = "inputLine">
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Email"
              type="email"
              fullWidth
            />
             <TextField
              autoFocus
              margin="dense"
              id="name"
              label="password"
              type="password"
              fullWidth
            />
            </div>
            <div className = "terms"><p>I agree to the Terms & Conditions & Privacy Policy</p> </div>
          </DialogContent>
          <DialogActions>
            <button className = "btn btn-primary btn-sm login" onClick={this.handleClose} color="primary">
              Login
            </button>
          </DialogActions>
        </Dialog>
      </div>
    <header>
        <nav id = "navbar" class = "navbar">
       
            <div class = "select-overlay"></div>
            <div class = "primary-header">
                <div class = "header-col header-col-1" >
                      <div class ="brand">
                        <NavLink to ="/"> <a class = "logo"> <img src = {require("../images/Screenshot (78).png")}></img></a></NavLink>
                      </div>
                </div>
                <div class = "header-col header-col-2">
                    <div class = "search-section">
                       <NavLink to = "/quickbook"><span class = "__icon search-icon">
                            <span><a class = "search_logo"><img src = {require("../images/search.svg")}></img></a></span>
                            <span><input class ="search_border" type = "search" placeholder = "Search for Movies, Events, Plays and Sports"></input></span>
                         
                         </span>
                        </NavLink> 
                      
                    </div>
                    <div class = "primary-nav">
                        <div class = "inner-nav left-nav">
                            <ul>
                               <NavLink to ="/movies" style={{textDecoration: 'none'}} activeStyle={{color: 'initial', textDecoration: 'none'}}><li class = "primary-menu"><a >Movies</a></li></NavLink> 
                                <li class = "primary-menu"><a >Events</a></li>
                                <li class = "primary-menu"><a >Plays</a></li>
                                <li class = "primary-menu"><a >Sports</a></li>
                                <li class = "primary-menu"><a >Monuments</a></li>
                                <li class = "primary-menu"><a >International</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class = "header-col header-col-3">
                        <button  type="button" class="btn btn-primary btn-sm" onClick={this.handleClickOpen}>Sign in</button>
                       <img onClick={this.toggleDrawer('right', true)}  className = "threeLines"  src = {require("../images/threeLinesd.png")}></img>
                </div>
            </div>
         
         
        </nav>
        
    </header>
    
</div>
        )
    };
}

export default Navbar;