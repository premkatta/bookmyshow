import React, { Component } from 'react';
import Navbar from './navbar';
import Carousel from './carousel';
import MovieCards from './movieCards';
import FooterPanel from './footerPanel';
import Theatres from './theatres';

import Seatbooking from './seatbooking';



class Home extends Component {
  render() {
    return (
     
      <div className="App">   
          <Navbar/>
          <Carousel/>
          <MovieCards/>
          <FooterPanel/>
          {/* <Seatbooking/>   */}
      </div>
      
       
    );
  }
}

export default Home;
