import React ,{Component} from 'react';
import '../css/carousel.css'

class Carousel extends Component{
    render(){
        return(
            <div class =  "carouselCards" >
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
  {/* <li data-target="#carouselExampleIndicators" data-slide-to="-2"></li>
  <li data-target="#carouselExampleIndicators" data-slide-to="-1"></li> */}
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class = "active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="https://in.bmscdn.com/showcaseimage/eventimage/cirque-du-soleil--bazzar-29-10-2018-01-44-58-992.jpg" alt="First slide"/>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="https://in.bmscdn.com/showcaseimage/eventimage/citibank-buy-one-get-one-offer-on-world-debit-cards-showcase-26-02-2017-11-23.jpg" alt="Second slide"/>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="https://in.bmscdn.com/showcaseimage/eventimage/pihu-07-11-2018-12-11-48-502.jpg" alt="Third slide"/>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
            </div>
        )
    }
}

export default Carousel;