import React , {Component} from 'react';
import '../css/seatbooking.css';


import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {NavLink} from 'react-router-dom';


import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import { red } from '@material-ui/core/colors';
import axios from 'axios';


let seats24= [{"alp":"A","num":1,"category":"Diamond"},{"alp":"B","num":2,"category":"Gold"},{"alp":"C","num":3,"category":"Silver"},{"alp":"D","num":4},{"alp":"E","num":5},{"alp":"F","num":6},{"alp":"G","num":7},{"alp":"H","num":8},{"alp":"I","num":9},{"alp":"J","num":10},{"alp":"K","num":11},{"alp":"L","num":12},{"alp":"M","num":13},{"alp":"N","num":14},{"alp":"O","num":15},{"alp":"P","num":16},{"alp":"Q","num":17},{"alp":"R","num":18},{"alp":"S","num":19},{"alp":"T","num":20},{"alp":"U","num":21},{"alp":"V","num":22},{"alp":"W","num":23},{"alp":"X","num":24}];
   const styles = {
        list: {
        width: 250,
        },
        fullList: {
        width: 'auto',
        },
    };

    
 
 
  
class Seatbooking extends Component{
  constructor(props) {
    super(props);
  this.state = {
    abc:[],
    activeRows: null,
    data: [{
        id: 1,
        seats: [{
            id: 10,
            occupied: true,
            currentlyBooking: false,
            seatNo: 1
        }, {
            id: 11,
            occupied: false,
            currentlyBooking: false,
            seatNo: 2
        }, {
            id: 12,
            occupied: true,
            currentlyBooking: false,
            seatNo: 3
        }, {
            id: 13,
            occupied: true,
            currentlyBooking: false,
            seatNo: 4
        }, {
            id: 14,
            occupied: false,
            currentlyBooking: false,
            seatNo: 5
        }]
    }],
    activeSeats: []
};
  }
 
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  
  state = {
    bottom: false,
     };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };
 
componentDidMount(){
  console.log('hello');
  var movie_name = this.props.match.params.movie_name;
  console.log(this.props);
  // debugger
  axios.get('https://myticketbooking.herokuapp.com'+movie_name).then((data)=>{
    this.setState({abc:data.data});
    console.log(this.state.abc);
    console.log('hello');
  })
}

ChangeColorClick(seatNo, id) {
console.log("You selected" , seatNo, id);
let idx = this.state.activeSeats.indexOf(seatNo+id);
let seats = this.state.activeSeats;
if (idx == -1) {
    seats.push(seatNo+id);
}
else if (idx > -1) {
    seats.splice(idx, 1);
}
this.setState({ activeSeats: seats });
}


    render(){
        const { classes } = this.props;

        const fullList = (
          <div className={classes.fullList}>
            <List className = "payRs">
                 <button type="button" class="btn btn-primary pay">Confirm Your Booking.</button>
            </List>
         
          </div>
        );

        return(
            <div >
            <Dialog
              className = "howmany"
              open={this.state.open}
              onClose={this.handleClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">{"How many seats?"}</DialogTitle>
              <DialogContent>
                <div><img  class ="vehicle" src = {require("../images/bike.svg")}/></div>
              </DialogContent>
              <DialogContent>
               <span className = "persons">
                   <div>1</div>
                   <div>2</div>
                   <div>3</div>
                   <div>4</div> 
               </span>
              </DialogContent>
              <DialogContent>
                    <span className = "price">
                        <div>
                            <ul>
                                <li>GOLD</li>
                                <li>Rs.175.00</li>
                                <li>Available</li>
                            </ul>
                        </div>
                        <div>
                            <ul>
                                <li>SILVER</li>
                                <li>Rs.175.00</li>
                                <li>Available</li>
                            </ul>
                        </div>
                    </span>
              </DialogContent>
              <DialogActions>
                <button className = "btn btn-primary btn-sm login" onClick={this.handleClose} color="primary">
                    Select seats
                </button>
              </DialogActions>
            </Dialog>
        
            <div><nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <NavLink to = {`/movies/${this.state.abc.length > 0 ? this.state.abc[0].title : "loading"}/theatres`} style={{color: 'black', textDecoration: 'none'}} activeStyle={{color: 'black', textDecoration: 'none'}}>
                <a class="navbar-brand" href="#"><a><img className = "svgLeftArrow" src = {require("../images/svgLeftArrow.svg")}/></a></a>
            </NavLink>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
              <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <div className= "seat_Title">
                        <ul>
                            <li>Aravinda Sametha Telugu</li>
                            <li className= "theatreName">Asian Mukunda</li>
                        </ul>
                    </div>
                </li>
              </ul>
              <form class="form-inline my-2 my-lg-0">
              <a className = "navbar-brand tickets"onClick={this.handleClickOpen}>n Tickets</a>
              <NavLink to = "/theatres"style={{color: 'black', textDecoration: 'none'}} activeStyle={{color: 'black', textDecoration: 'none'}}>
                    <a class="navbar-brand" href="#"><a >x</a></a>
              </NavLink>
              </form>
            </div>
          </nav></div>

          <div><nav class="navbar navbar-expand-lg navbar-light bg-light">
         
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
              <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <div className= "seat_timings">
                        <ul>
                            <li>Today
                                {/* <p><script> document.write(new Date().toDateString()); </script></p> */}
                            </li>
                            {/* <button type="button" class="btn btn-sm btn-outline-success showtime">Success</button>                        */}
                            {/* <button type="button" class="btn btn-sm btn-outline-success showtime">Success</button>                      */}
                            
                        </ul>
                    </div>
                </li>
              </ul>
            </div>
          </nav></div>

          <div className = "seatsCountPanel">
          <div className="proceedButton"><button className="btn btn-primary" onClick={this.toggleDrawer('bottom',true)}>Proceed</button></div>
            <div className= "seatsCount">
                <div className="seatCategory">
                DIAMOND-Rs. 175.00
                </div>
                <div className = "line"></div>
                {seats24.map((e,index)=>{
               if (index<2){
              return <tr>
                    <td className="rowName"> <a>{e.alp}</a> </td>
                    {seats24.map((el=>{
                        return  <td> <div className={` ${this.state.activeSeats.indexOf(e.alp+el.num) > -1 ? 'green' : ''} seatNumber`} onClick={() => this.ChangeColorClick(e.alp , el.num)}>{el.num}</div>  </td>;
                    }))}
                </tr>
               }
                })}
                 <div className="seatCategory">
                GOLD-Rs. 175.00
                </div>
                 <div className = "line"></div>
                 {seats24.map((e,index)=>{
               if (index>1 && index<6){
              return <tr>
                    <td className="rowName"> <a>
                      
                    {e.alp}
                    
                    </a> </td>
                   
                      {seats24.map((el)=>{
                      if(el.num>0 && el.num<12){
                        return  <td> <div  className={` ${this.state.activeSeats.indexOf(e.alp+el.num) > -1 ? 'green' : ''} seatNumber`} onClick={() => this.ChangeColorClick(e.alp , el.num)} >{el.num}</div> </td>;                      }
                      })}
                      <div class = "gold_dummyseat"> </div>
                   
                      {seats24.map((el)=>{
                      if(el.num>11 && el.num<23){
                        return  <td> <div  className={` ${this.state.activeSeats.indexOf(e.alp+el.num) > -1 ? 'green' : ''} seatNumber`} onClick={() => this.ChangeColorClick(e.alp , el.num)} >{el.num}</div> </td>;                      }
                      })}
                </tr>
               }
                })}
          <div className="seatCategory">
                SILVER-Rs. 175.00
                </div>
                 <div className = "line"></div>
                 {seats24.map((e,index)=>{
               if (index>5 && index<9){
              return <tr>
                     <td className="rowName"> <a>{e.alp}</a> </td>
                    {seats24.map((el)=>{
                      if(el.num>0 && el.num<8){
                        return  <td> <div  className={` ${this.state.activeSeats.indexOf(e.alp+el.num) > -1 ? 'green' : ''} seatNumber`} onClick={() => this.ChangeColorClick(e.alp , el.num)} >{el.num}</div> </td>;                      }
                      })}
                      <div class = "silver_dummyseat"> </div>
                    {seats24.map((el)=>{
                      if(el.num>7 && el.num<15){
                        return  <td> <div  className={` ${this.state.activeSeats.indexOf(e.alp+el.num) > -1 ? 'green' : ''} seatNumber`} onClick={() => this.ChangeColorClick(e.alp , el.num)} >{el.num}</div> </td>;
                      }
                      })}
                      <div class = "silver_dummyseat"> </div>
                      {seats24.map((el)=>{
                      if(el.num>14 && el.num<22){
                        return  <td> <div className={` ${this.state.activeSeats.indexOf(e.alp+el.num) > -1 ? 'green' : ''} seatNumber`} onClick={() => this.ChangeColorClick(e.alp , el.num)} >{el.num}</div>  </td>;
                      }
                      })}
      
                </tr>
               }
                })}
            </div>
            <div className ="screenIconDiv" ><svg className="screenIcon" xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 262 22" id="icon-screen" width="100%" height="100%"><g fill="none" fill-rule="evenodd" opacity=".3"><g fill="#E1E8F1"><path id="da" d="M27.1 0h205.8L260 14.02H0z"></path></g><path stroke="#4F91FF" stroke-width=".65" d="M27.19.33L1.34 13.7h257.32L232.81.32H27.2z"></path><path fill="#8FB9FF" d="M28.16 2.97h203.86l17.95 9.14H10.35z"></path><g fill="#E3ECFA"><path id="db" d="M0 13.88h260l-3.44 6.06H3.44z"></path></g><path stroke="#4F91FF" stroke-width=".65" d="M.56 14.2l3.07 5.41h252.74l3.07-5.4H.56z"></path></g></svg><p>All eyes this way please!</p></div>
          </div>
          <div>
        <Drawer
          anchor="bottom"
          open={this.state.bottom}
          onClose={this.toggleDrawer('bottom', false)}
        >
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer('bottom', false)}
            onKeyDown={this.toggleDrawer('bottom', false)}
          >
            {fullList}
          </div>
        </Drawer>
       
      </div>

          </div>
        )
    }
};


Seatbooking.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(Seatbooking);