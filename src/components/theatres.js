import React , {Component} from 'react';
import '../css/theatres.css';
import SimpleExpansionPanel from './materialUItest';
import Navbar from './navbar';
import {NavLink} from 'react-router-dom';
import FooterPanel from './footerPanel';
import axios from 'axios';

class Theatres extends Component{

    constructor(props){
        super(props);
        this.state={abc:[],theatresList:[]}
    }
    componentWillMount(){
        var movie_name=this.props.match.params.movie_name;
        var theatre_name=this.props.match.params.theatre_name;
        axios.get('http://myticketbooking.herokuapp.com/cinema/'+movie_name).then((data)=>{
            this.setState({abc:data.data});
            console.log(this.state.abc);
        })
        axios.get('http://myticketbooking.herokuapp.com/'+movie_name+'theatres'+theatre_name).then((data)=>{
            this.setState({theatresList:data.data});
            console.log(this.theatresList);
        })
    }
  
    
 render(){   
    // const titleCardStyle={
    //     display:'none',
    // }
   return(
       <div>
           <Navbar/>
    <div className = "thetrePanel">
        <div className = "titleCard">
        <img  className = "titleCardImg" src = {this.state.abc.length>0?this.state.abc[0].trailer_img:"loading"}/>
          <div className = "theatreTitleInfo ">
            <p class = "theatreTitle">{this.state.abc.length>0?this.state.abc[0].title:"loading"}</p>   
            <div>
                <span>
                    <a className = "censor">U/A</a>
                    <a className ="buttonMode">
                        <span><a>COMEDY</a></span>
                        <span><a>FANTASY</a></span>
                        <span><a>THRILLER</a></span>
                    </a>
                    <div className = "mDate2">
                        <span><i class="far fa-calendar-alt"></i>{this.state.abc.length>0?this.state.abc[0].releaseDate:"loading"}</span>
                        <span><i class="far fa-clock"></i>{this.state.abc.length>0?this.state.abc[0].duration:"loading"}</span>
                    </div>
                </span>
            </div>
            </div>
         
          <div className = "crew col-md-6">
            <div className="actorsDiv">
                <div class = "dirInfo">
                    <div>DIRECTOR </div>
                    <div>
                     
                        {/* {this.state.abc[0].crew_data} */}
                        <div><img src = 'https://in.bmscdn.com/iedb/artist/images/website/poster/large/trivikram-srinivas-5707-19-09-2017-12-59-06.jpg'/></div>
                        <div>Trikram</div>
                    </div>
                </div>
                <div class = "crewInfo">
                <div>CREW</div>
                { this.state.abc.length>0?this.state.abc[0].crew_data.map((e)=>{ 
                    return <div class="crewInfo_person">
                                <div><img src ={e.crew_img}/></div>
                                <div className="crew_name">{e.crew_name}</div>
                            </div>
                    
                    }):""
            }
                </div>
            </div>          
          </div>
        </div>
       <div className = "abb">
        <div class = "theatreSchedulePanel row">
            <div class = "col-md-6 daySchedule borderRight">
                <div className = "dayScheduleDate">
                    <div className = "datenumber">4</div>
                    <div className = "day">TODAY</div>
                </div>
                <div className = "dayScheduleDate">
                <div className = "datenumber">4</div>
                    <div className = "day">TOM</div>
                </div>
                <div className = "dayScheduleDate">
                <div className = "datenumber">4</div>
                    <div className = "day">THU</div>
                </div>
            </div>
            <span className = "col-md-2 borderRight">
                <div className = "theatreFilter">search</div>
                <span>v</span>
            </span>
            <span className = "col-md-2 borderRight">
            {/* <SimpleExpansionPanel></SimpleExpansionPanel> */}
                <div>search</div>
            </span>
            <span className = "col-md-2 borderRight">
                <div>search</div>
            </span>
           </div>
        <div className = "theatreListPanel">       
         <div className = "theatreList">
            <div className = "theatreItem">
                <div className = "thetreBox">
                    <div className = "heart"><img src = {require("../images/heart.svg")}/>  </div>
                    <div className = "theatreName">Santhosh Cinaimas</div>
                    <div className = "infoIcon"><img src= {require("../images/info.svg")}/></div>
                    <div>
                        <div className="phoneIco"  ><svg xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-mobile" width="100%" height="100%"><path d="M73.5 95.2H26.8c-1.3 0-2.3-1-2.3-2.3V7.6c0-1.3 1-2.3 2.3-2.3h46.7c1.3 0 2.3 1 2.3 2.3V93c0 1.2-1.1 2.2-2.3 2.2zM26.8 6.4c-.6 0-1.1.5-1.1 1.1v85.4c0 .6.5 1.1 1.1 1.1h46.7c.6 0 1.1-.5 1.1-1.1V7.6c0-.6-.5-1.1-1.1-1.1l-46.7-.1z"></path><path d="M68.8 78.9H31.3c-.6 0-1.2-.5-1.2-1.2V16c0-.6.5-1.2 1.2-1.2h37.5c.6 0 1.2.5 1.2 1.2v61.7c0 .6-.5 1.2-1.2 1.2zm0-62.9H31.3v61.6h37.4l.1-61.6zm-19 74.4c-2.3 0-4.1-1.9-4.1-4.1 0-2.3 1.9-4.1 4.1-4.1 2.3 0 4.1 1.9 4.1 4.1a4 4 0 0 1-4.1 4.1zm0-7.1a2.9 2.9 0 1 0 2.9 2.9c.1-1.5-1.2-2.9-2.9-2.9zm-5.5-72.1h-1.6c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h1.6c.3 0 .6.3.6.6s-.3.6-.6.6zm11 0h-6.5c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h6.5c.3 0 .6.3.6.6s-.2.6-.6.6z"></path></svg></div>
                        <div className="mTicket">M-Ticket</div>
                    </div>
                </div>
                <NavLink to ={`/movies/${this.state.abc.length > 0 ? this.state.abc[0].title : "loading"}/theatres/seatbooking`} style={{textDecoration: 'none'}} activeStyle={{color: 'initial', textDecoration: 'none'}}>         
                <div className = "timingBox">
                    <span>
                        <a className ="theatreTiming">
                            <span><a>10.45 AM</a></span>
                        </a> 
                        <a className ="theatreTiming">
                        <span><a>2.45 PM</a></span>
                        </a>
                        <a className ="theatreTiming">
                            <span><a>5.45 PM</a></span>
                        </a>
                    </span>
                    <div>
                        <div className="cancelCircle"></div>
                        <span>Cancellation available.</span> 
                   </div>
                </div>
                </NavLink>
                
            </div>    

                    <div className = "theatreItem">
                <div className = "thetreBox">
                    <div className = "heart"><img src = {require("../images/heart.svg")}/>  </div>
                    <div className = "theatreName">Santhosh Cinaimas</div>
                    <div className = "infoIcon"><img src= {require("../images/info.svg")}/></div>
                    <div>
                        <div className="phoneIco"  ><svg xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-mobile" width="100%" height="100%"><path d="M73.5 95.2H26.8c-1.3 0-2.3-1-2.3-2.3V7.6c0-1.3 1-2.3 2.3-2.3h46.7c1.3 0 2.3 1 2.3 2.3V93c0 1.2-1.1 2.2-2.3 2.2zM26.8 6.4c-.6 0-1.1.5-1.1 1.1v85.4c0 .6.5 1.1 1.1 1.1h46.7c.6 0 1.1-.5 1.1-1.1V7.6c0-.6-.5-1.1-1.1-1.1l-46.7-.1z"></path><path d="M68.8 78.9H31.3c-.6 0-1.2-.5-1.2-1.2V16c0-.6.5-1.2 1.2-1.2h37.5c.6 0 1.2.5 1.2 1.2v61.7c0 .6-.5 1.2-1.2 1.2zm0-62.9H31.3v61.6h37.4l.1-61.6zm-19 74.4c-2.3 0-4.1-1.9-4.1-4.1 0-2.3 1.9-4.1 4.1-4.1 2.3 0 4.1 1.9 4.1 4.1a4 4 0 0 1-4.1 4.1zm0-7.1a2.9 2.9 0 1 0 2.9 2.9c.1-1.5-1.2-2.9-2.9-2.9zm-5.5-72.1h-1.6c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h1.6c.3 0 .6.3.6.6s-.3.6-.6.6zm11 0h-6.5c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h6.5c.3 0 .6.3.6.6s-.2.6-.6.6z"></path></svg></div>
                        <div className="mTicket">M-Ticket</div>
                    </div>
                </div>
                <NavLink to ="/seatbooking" style={{textDecoration: 'none'}} activeStyle={{color: 'initial', textDecoration: 'none'}}>          
                <div className = "timingBox">
                    <span>
                        <a className ="theatreTiming">
                            <span><a>10.45 AM</a></span>
                        </a> 
                        <a className ="theatreTiming">
                        <span><a>2.45 PM</a></span>
                        </a>
                        <a className ="theatreTiming">
                            <span><a>5.45 PM</a></span>
                        </a>
                    </span>
                    <div>
                        <div className="cancelCircle"></div>
                        <span>Cancellation available.</span> 
                   </div>
                </div>
                </NavLink>
                
            </div>  
            <div className = "theatreItem">
                <div className = "thetreBox">
                    <div className = "heart"><img src = {require("../images/heart.svg")}/>  </div>
                    <div className = "theatreName">Mukunda Cinaimas</div>
                    <div className = "infoIcon"><img src= {require("../images/info.svg")}/></div>
                    <div>
                        <div className="phoneIco"  ><svg xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-mobile" width="100%" height="100%"><path d="M73.5 95.2H26.8c-1.3 0-2.3-1-2.3-2.3V7.6c0-1.3 1-2.3 2.3-2.3h46.7c1.3 0 2.3 1 2.3 2.3V93c0 1.2-1.1 2.2-2.3 2.2zM26.8 6.4c-.6 0-1.1.5-1.1 1.1v85.4c0 .6.5 1.1 1.1 1.1h46.7c.6 0 1.1-.5 1.1-1.1V7.6c0-.6-.5-1.1-1.1-1.1l-46.7-.1z"></path><path d="M68.8 78.9H31.3c-.6 0-1.2-.5-1.2-1.2V16c0-.6.5-1.2 1.2-1.2h37.5c.6 0 1.2.5 1.2 1.2v61.7c0 .6-.5 1.2-1.2 1.2zm0-62.9H31.3v61.6h37.4l.1-61.6zm-19 74.4c-2.3 0-4.1-1.9-4.1-4.1 0-2.3 1.9-4.1 4.1-4.1 2.3 0 4.1 1.9 4.1 4.1a4 4 0 0 1-4.1 4.1zm0-7.1a2.9 2.9 0 1 0 2.9 2.9c.1-1.5-1.2-2.9-2.9-2.9zm-5.5-72.1h-1.6c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h1.6c.3 0 .6.3.6.6s-.3.6-.6.6zm11 0h-6.5c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h6.5c.3 0 .6.3.6.6s-.2.6-.6.6z"></path></svg></div>
                        <div className="mTicket">M-Ticket</div>
                    </div>
                </div>
                <NavLink to ="/seatbooking" style={{textDecoration: 'none'}} activeStyle={{color: 'initial', textDecoration: 'none'}}>          
                     <div className = "timingBox">
                    <span>
                        <a className ="theatreTiming">
                            <span><a>10.45 AM</a></span>
                        </a> 
                        <a className ="theatreTiming">
                        <span><a>2.45 PM</a></span>
                        </a>
                        <a className ="theatreTiming">
                            <span><a>5.45 PM</a></span>
                        </a>
                    </span>
                    <div>
                        <div className="cancelCircle"></div>
                        <span>Cancellation available.</span> 
                   </div>
                </div>
                </NavLink>
                
            </div>         
            <div className = "latestMovies">
                <p>LATEST MOVIES IN HYDERABAD  </p>
                <p><a>Antariksham 9000KMPH</a>, <a>Padi Padi Leche Manasu</a>, <a>Anaganaga O Premakatha</a></p>
            </div>
            <div className = "privacy">
                <p>Privacy Note</p>
                <p>By using www.bookmyshow.com(our website), you are fully accepting the Privacy Policy available at<a href = "https://bookmyshow.com/privacy" > https://bookmyshow.com/privacy </a>  governing your access to Bookmyshow and provision of services by Bookmyshow to you. If you do not accept terms mentioned in the <a href= "https://in.bookmyshow.com/privacy">Privacy Policy</a>, you must not share any of your personal information and immediately exit Bookmyshow.</p>
            </div>
            
           </div>
        </div>
             
    </div>
    </div>
    <FooterPanel/>
</div>
        
  )
 }
}

export default Theatres;
