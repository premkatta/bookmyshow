import React , { Component } from 'react';
import '../css/footerPanel.css';

class FooterPanel extends Component{
    render(){
        return(
            <div className = "footerPanel">
                <div className = "supportSection">
                <div className = "row">
                    <div className = "col-md-4 custSupport">
                    <div><img src = {require('../images/cust-support.svg')}></img></div>
                        <a>24/7 CUSTOMER CARE</a>
                    </div>
                    <div className = "col-md-4 custSupport">
                    <div><img src = {require('../images/tickets.svg')}></img></div>
                        <a>RESEND BOOKING CONFIRMATION</a>
                    </div>
                    <div className = "col-md-4 custSupport">
                    <div><img src = {require('../images/newsletter.svg')}></img></div>
                        <a>SUBSCRIBE TO THE NEWSLETTER</a>
                    </div>
                </div>
                </div>
                <div className = "locationBookings">
                    <span>
                    <a>Online Movie Ticket Booking Mumbai</a> |  <a>Online Movie Ticket Booking Mumbai</a> |  <a>Online Movie Ticket Booking Mumbai</a> |  <a>Online Movie Ticket Booking Mumbai</a> |  <a>Online Movie Ticket Booking Mumbai</a> 
                    </span>
                </div>
                <div className ="footerContent">
                    <div className = "footerLinks row">
                            <div className = "col-md-3">
                              <ul>
                                  <div className = "liTitle">
                                <li>BEST LANGUAGE GENRE MOVIES</li>
                                   </div>  
                                <li><a>Telugu action Movies</a> | <a>Telugu comedy movies</a></li>
                                <li><a>Telugu action Movies</a> | <a>Telugu comedy movies</a></li>
                              </ul>
                            </div>
                            <div className = "col-md-3">
                              <ul>
                                  <div className = "liTitle">
                                <li>BOOKMYSHOW APP</li>
                                   </div>  
                                <li><a> Download BMS Ticketing iOS App</a> </li>
                                <li><a> Download BMS Ticketing Android App</a></li>
                              </ul>
                            </div>
                            <div className = "col-md-3">
                              <ul>
                                  <div className = "liTitle">
                                <li>BEST LANGUAGE GENRE MOVIES</li>
                                   </div>  
                                <li><a>Telugu action Movies</a> | <a>Telugu comedy movies</a></li>
                                <li><a>Telugu action Movies</a> | <a>Telugu comedy movies</a></li>
                              </ul>
                            </div>
                            <div className = "col-md-3">
                              <ul>
                                  <div className = "liTitle">
                                <li>HELP</li>
                                   </div>  
                                <li><a>Privacy policy</a></li>
                                <li><a>Terms and conditions</a></li>
                              </ul>
                            </div>
                    </div>
                    <div className = "footerEnd">
                        <div className = "bmsLogo">
                            <span>
                                <img src = {require("../images/bmsLogofooter.png")}/>
                            </span>
                        </div>
                        <div className = "socialIcons">
                        <div className = "iconCircle" >
                           <a href = "https://www.facebook.com/BookMyShowIN" target = "blank"><i class = "fa fa-facebook"></i></a>     
                        </div> 
                        <div className = "iconCircle" >
                            <a href = "https://twitter.com/BookMyShow/" target = "blank">  <i class = "fa fa-twitter"></i></a>   
                        </div>
                        <div className = "iconCircle" >
                            <a href = "https://www.instagram.com/bookmyshowin/" target = "blank"><i class = "fa fa-instagram"></i></a>    
                        </div>
                        <div className = "iconCircle" >
                            <a href = "https://www.youtube.com/user/BookMyShow/featured" target = "blank"><i class = "fa fa-youtube-play"></i></a>    
                        </div>
                        <div className = "iconCircle" >
                            <a href = "https://in.pinterest.com/bookmyshow/" target = "blank"><i class = "fa fa-pinterest-p"></i></a>    
                        </div>
                        </div>
                    </div>
                </div>
                <div className = "wrapperContent">
                <p>Copyright 2018 © Bigtree Entertainment Pvt. Ltd. All Rights Reserved.</p>
                <p>The content and images used on this site are copyright protected and copyrights vests with the respective owners. The usage of the content and images on this website is intended to promote the works and no endorsement of the artist shall be implied. </p>
                <p>Unauthorized use is prohibited and punishable by law.</p>
                </div>
            </div>
        );
    }
}
export default FooterPanel;