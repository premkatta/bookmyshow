import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';

import '../css/welcome.css';

const styles = ({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  }
});

class Welcome extends React.Component {
  state = {
    right: false,
  };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  render() {
    const { classes } = this.props;
    const sideList = (
    <div className={classes.list}>
        <div className = "Guest">
          <span class = "userIcon">
            <img src = {require("../images/user.svg")}/>
          </span>
          <span class ="welGuest">
            <p className = "wg">Welcome Guest!</p>
            <p>Sign In / Sign Up</p>
          </span>
        </div>
          <div>
          <List >
            <ListItem button >
              Notifications
            </ListItem>
          </List>
          <List>
            <ListItem button >
              Support
            </ListItem>
          </List>
          <List>
            <ListItem button >
              Settings
            </ListItem>
          </List>
          <Divider/>  
        </div>
    </div>
    );

    return (
      <div>
        <Button onClick={this.toggleDrawer('right', true)}>---</Button>
        <Drawer anchor="right" open={this.state.right} onClose={this.toggleDrawer('right', false)}>
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer('right', false)}
            onKeyDown={this.toggleDrawer('right', false)}
          >
            {sideList}
          </div>
        </Drawer>
      </div>
    );
  }
}


Welcome.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Welcome);
